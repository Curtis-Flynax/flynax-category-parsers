<?php

/**
 * Category scraper from the https://cars.av.by website
 * #QEB-387259
 */

$GLOBALS['start_time']   = gettime();
$GLOBALS['start_memory'] = memory_get_usage();

use Symfony\Component\Panther\Client;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

set_time_limit(0);

require_once dirname(__DIR__) . '/includes/config.inc.php';
require_once RL_INC . '/control.inc.php';
$config = $rlConfig->allConfig();

require __DIR__ . '/vendor/autoload.php';

$parserConfig = [
    'url'     => 'https://cars.av.by',
    '1_level' => [
        'selector'        => '.catalog__list a', // CSS selector of values in 1 level categories
        'loadButton'      => '.catalog__show-all button', // CSS selector of button which must be clicked to load full list of values (if it's not loaded or hidden)
        'lastHrefElement' => '/suzuki', // Href of last necessary element in list [optional]
    ],
    '2_level' => [
        'selector' => '.catalog__list a', // CSS selector of values in 2 level categories
    ],
    '3_level' => [
        'selector'   => '.dropdown-listbox.dropdown-listbox--generation .dropdown__card-text', // CSS selector of values in 3 level categories
        'loadButton' => 'button[name=p-6-0-4-generation]',
    ],
];

$client = Client::createChromeClient();
$clientGoutte = new Symfony\Component\BrowserKit\HttpBrowser();
$categories = [];

if ($parserConfig['1_level']) {
    $client->request('GET', $parserConfig['url']);
    $categories = getElementValues($parserConfig, 1, $client);
    $client->quit();

    foreach ($categories as &$category) {
        $crawler = $clientGoutte->request('GET', $category['link']);
        $category['subCategories_2'] = getElementValuesViaGoutte($parserConfig, 2, $crawler, $category);

        foreach ($category['subCategories_2'] as &$subCategory) {
            $crawler = $clientGoutte->request('GET', $subCategory['link']);
            $subCategory['subCategories_3'] = getElementValuesViaGoutte($parserConfig, 3, $crawler, $subCategory);
        }
    }
}

saveInXLSX($categories);

echo "<pre>";
echo 'Categories successfully parsed! Check file: parsed-categories.xlsx';

$stop_time = gettime();
$diff_time = bcsub($stop_time, $GLOBALS['start_time'], 6);

$stop_memory = memory_get_usage();
$diff_memory = $stop_memory - $GLOBALS['start_memory'];
$unit = array('b','kb','mb','gb','tb','pb');
$diff_memory = round($diff_memory/pow(1024,($i=floor(log($diff_memory,1024)))),2) . ' ' . $unit[$i];

echo "<br><br>";
echo 'Time: ' . $diff_time . ' sec<br>';
echo 'Memory: ' . $diff_memory . '<br>';
echo 'Peak of memory usage: ' . round((memory_get_peak_usage() / 1024 / 1024), 2) . ' mb';
exit;

function gettime()
{
    $start_time = explode(' ',microtime());
    $real_time = $start_time[1].substr($start_time[0],1);
    return $real_time;
}

function getElementValuesViaGoutte($parserConfig, $level, $crawler, $category = null): array
{
    try {
        $levelData = $parserConfig[$level . '_level'];
        $selector  = $levelData['selector'];
        $data = $crawler->filter($selector)->each(function ($node) use ($parserConfig, $level, $category) {
            $title = $node->attr('title') ?: $node->text();
            $path  = $node->attr('href') ?: '';

            if ($level === 3) {
                $title = preg_replace("/(…)?$/u", '', $title);
                $path = adaptCategoryPath($category['path'] . '/' . $GLOBALS['rlValid']->str2path($title));
                return ['title' => $title, 'path' => $path];
            }

            $link = false !== strpos($path, 'http') ? $path : ($path ? $parserConfig['url'] . $path : '');
            $path = $category['path'] . '/' . adaptCategoryPath($GLOBALS['rlValid']->str2path($title));
            return ['title' => $title, 'path' => $path, 'link' => $link];
        });
        sort($data);
        return $data;
    } catch (Exception $exception) {
        return [];
    }
}

/**
 * Replace "-" in end of path to "+"
 *
 * @param string $path
 *
 * @return string
 */
function adaptCategoryPath(string $path): string
{
    if (preg_match('/-\d+$/', $path)) {
        $path = preg_replace("/(-)(\d+)$/", '$2', $path);
        $path = adaptCategoryPath($path);
    }

    return $path;
}

/**
 * Slow alternative which allow click to buttons for loading full list
 * @param array  $parserConfig
 * @param int    $level
 * @param Client $client
 *
 * @return array
 */
function getElementValues(array $parserConfig, int $level, Client $client): array
{
    try {
        $levelData = $parserConfig[$level . '_level'];
        $selector  = $levelData['selector'];

        if (isset($levelData['loadButton'])) {
            $client->executeScript("document.querySelector('{$levelData['loadButton']}').click()");
        }

        if (isset($levelData['lastHrefElement'])) {
            $crawler = $client->waitFor("//a[@href='{$levelData['lastHrefElement']}']");
        } else {
            $crawler = $client->waitFor($selector);
        }

        $data = $crawler->filter($selector)->each(function ($node) use ($parserConfig) {
            $title = $node->attr('title') ?: $node->text();
            $path  = $node->attr('href');
            $link  = false !== strpos($path, 'http') ? $path : ($path ? $parserConfig['url'] . $path : '');
            return ['title' => $title, 'path' => adaptCategoryPath($GLOBALS['rlValid']->str2path($title)), 'link' => $link];
        });
        sort($data);
        return $data;
    } catch (Exception $exception) {
        return [];
    }
}

/**
 * @throws \PhpOffice\PhpSpreadsheet\Exception
 * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
 */
function saveInXLSX(array $categories): void
{
    $spreadsheet = new Spreadsheet();
    $activeWorksheet = $spreadsheet->getActiveSheet();
    $index = 1;

    // Set main row with names of columns
    $activeWorksheet->fromArray(['Name', 'Parent', 'Path', 'Title', 'H1', 'Meta_description', 'Meta_keywords', 'Lock'], NULL, 'A' . $index);
    $activeWorksheet->getStyle('A' . $index . ':' . $activeWorksheet->getHighestDataColumn() . '1')->getFont()->setBold(true);
    $index++;

    foreach ($categories as $category) {
        $activeWorksheet->fromArray([$category['title'], '', $category['path']], NULL, 'A' . $index);
        $index++;

        if ($category['subCategories_2']) {
            foreach ($category['subCategories_2'] as $subCategory2) {
                $activeWorksheet->fromArray([$subCategory2['title'], $category['title'], $subCategory2['path']], NULL, 'A' . $index);
                $index++;

                if ($subCategory2['subCategories_3']) {
                    foreach ($subCategory2['subCategories_3'] as $subCategory3) {
                        $activeWorksheet->fromArray([$subCategory3['title'], $subCategory2['title'], $subCategory3['path']], NULL, 'A' . $index);
                        $index++;
                    }
                }
            }
        }
    }

    $writer = new Xlsx($spreadsheet);
    $writer->save('parsed-categories.xlsx');
}
