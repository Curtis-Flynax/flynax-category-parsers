<h1>Installation:</h1>

1. open terminal in current folder run command: `composer install`
2. for ubuntu run command: `apt-get install chromium-chromedriver firefox-geckodriver`
3. for mac run command: `brew install chromedriver geckodriver`
4. for windows run command: `choco install chromedriver selenium-gecko-driver`

<h2>Usage:</h2>


<b>Dom Crawler</b>: https://symfony.com/doc/current/testing/dom_crawler.html
