<?php

use Flynax\Utils\ListingMedia;
use Flynax\Component\ListingImageUploader;

set_time_limit(0);

define('REALM', true);

/* load system config */
require_once '../includes' . DIRECTORY_SEPARATOR . 'config.inc.php';

/* system controller */
require_once RL_INC . 'control.inc.php';

require_once 'vendor/autoload.php';

require_once RL_PLUGINS . 'xmlFeeds/phpQuery/phpQuery.php';

$languages = $rlLang->getLanguagesList();

$site_url = 'https://www.sonax.msk.ru';
$root = $reefless->getPageContent($site_url);

$reefless->loadClass('Listings');

$config['img_crop_module'] = 0;
$config['img_crop_thumbnail'] = 0;
$config['watermark_image_url'] = RL_FILES . 'watermark/logo-goods.png';

phpQuery::newDocument($root);

$position = 1;
$categories = pq('.pfilters a');

foreach ($categories as $category) {
    $parent_category_name = pq($category)->text();
    $href = pq($category)->attr('href');
    $uid = str_replace('.html', '', $href);
    $uid = ltrim($uid, '/');
    $key = str_replace('-', '_', $uid);

    $parent_category_url = $site_url . $href;
    
    $parent_category = [
        'Position' => $position,
        'Tree' => $position,
        'Key' => $key,
        'Path' => $uid,
        'Level' => 0,
        'Parent_ID' => 0,
        'Parent_keys' => '',
        'Type' => 'goods',
        'Count' => 0
    ];

    // $rlDb->insertOne($parent_category, 'categories');
    // $parent_id = $rlDb->insertID();

    $phrases[] = [
        'Module' => 'category',
        'Key' => 'categories+name+' . $key,
        'Value' => $parent_category_name
    ];

    echo '<b>Categrory</b>: ' . $parent_category_url .'<br /><br />';

    if ($href) {
        $category_html = $reefless->getPageContent($parent_category_url);
        phpQuery::newDocument($category_html);

        $child_position = 1;
        $sub_categories = pq('.pfilters:first a:not(:first)');

        foreach ($sub_categories as $sub_category) {
            $child_category_name = pq($sub_category)->text();
            $child_category_name = trim($child_category_name);

            $child_href = pq($sub_category)->attr('href');
            $child_uid = str_replace('.html', '', $child_href);
            $child_uid = ltrim($child_uid, '/');
            $child_key = str_replace('-', '_', $child_uid);

            $child_category_url = $site_url . $child_href;

            $child_category = [
                'Position' => $child_position,
                'Tree' => $position .'.'. $child_position,
                'Key' => $child_key,
                'Path' => $child_uid,
                'Level' => 1,
                'Parent_ID' => $parent_id,
                'Parent_IDs' => $parent_id,
                'Parent_keys' => $key,
                'Type' => 'goods',
                'Count' => 0
            ];

            // $rlDb->insertOne($child_category, 'categories');
            // $category_id = $rlDb->insertID();

            $categoru_sql = "
                SELECT `T1`.`ID`, `T1`.`Key` FROM `{db_prefix}categories` AS `T1`
                LEFT JOIN `{db_prefix}lang_keys` AS `T2` ON CONCAT('categories+name+', `T1`.`Key`) = `T2`.`Key`
                WHERE `T2`.`Value` = '{$child_category_name}' AND `T2`.`Module` = 'category' LIMIT 1
            ";
            $category_id = $rlDb->getOne($categoru_sql);
            var_dump($category_id);
            exit;

            $phrases[] = [
                'Module' => 'category',
                'Key' => 'categories+name+' . $child_key,
                'Value' => $child_category_name
            ];

            echo '<b>SUB Categrory</b>: ' . $child_category_url .'<br /><br />';

            $child_category_html = $reefless->getPageContent($child_category_url);
            phpQuery::newDocument($child_category_html);

            $goods_data = pq('body > div.row div.card');

            foreach ($goods_data as $good) {
                $title = pq($good)->find('p.pgoodsp')->text();
                
                $good_href = pq($good)->find('> a')->attr('href');

                $good_url = $site_url . $good_href;
                $goods_html = $reefless->getPageContent($good_url);

                phpQuery::newDocument($goods_html);

                $price = pq('body > .container .col-xl-4.col-lg-5 .h1.m-0.p-0 strong')->text();
                $description = pq('body > .container article.mmpp');
                $description->find('*:not(p)')->remove();
                $description->find('p:contains("Москв")')->remove();

                $listing = [
                    'Category_ID' => $category_id,
                    'Account_ID' => '37',
                    'Plan_type' => 'listing',
                    'Plan_ID' => 39,
                    'Pay_date' => 'NOW()',
                    'Photos_count' => 1,
                    'Date' => 'NOW()',
                    'Sub_status' => 'visible',
                    'Status' => 'active',
                    'good_title' => $title,
                    'price' => round($price / 4.28, -1) . '|currency_mdl',
                    'good_description' => trim($description->text()),
                ];

                $options = pq('body > .container table.ptable tr');

                foreach ($options as $option) {
                    $option_name = pq($option)->find('td:first')->text();
                    $option_value = pq($option)->find('td:last')->text();
                    
                    switch ($option_name) {
                        case 'Код товара':
                            $listing['ref_number'] = $option_value;
                            break;
                        
                        case 'Страна-производитель':
                            $listing['country'] = 'countries_germany';
                            break;
                        
                        case 'Бренд':
                            $listing['good_brand'] = 'good_brand_sonax';
                            break;
                        
                        case 'Объем':
                            if ($option_value != 'н.д.') {
                                $listing['good_volume'] = intval($option_value) . '|good_volume_mlitter';
                            }
                            break;
                    }
                }

                $rlDb->rlAllowHTML = true;
                $rlDb->insertOne($listing, 'listings');
                $listing_id = $rlDb->insertID();

                $photos = [];
                if ($photos[] = pq('body > .container #nav-tabContent div > a')->attr('href')) {
                    (new ListingImageUploader())->load($listing_id, $photos);
                }
            }

            $child_position++;
        }
    }

    $position++;
}

// foreach ($phrases as $phrase) {
//     foreach ($languages as $language) {
//         $insert_phrase = $phrase;
//         $insert_phrase['Code'] = $language['Code'];

//         $rlDb->insertOne($insert_phrase, 'lang_keys');
//     }
// }

echo 'Done';
